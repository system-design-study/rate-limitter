import requests
import time

def main():
    i = 0
    while i < 11:
        print("Request %i" % i)
        r = requests.get("http://localhost:8080/limited", headers={"X-Client-Type": "mobile", "X-Client-Token": "xxx"})
        print(r.text + "\n")
        i = i + 1
        time.sleep(2)

if __name__ == "__main__":
    main();
