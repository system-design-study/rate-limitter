## Rate Limiter

### Limiting requests
Sometimes, services might experience increased traffic spikes.
There are various reasons including: growing service popularity, load testing or malicious intent (someone wants to DDoS our service).

This leads to the so called **noisy neighbour problem** - one client consumes way too many resources (CPU, memory, IO) and affects the well-being of other clients.

Rate limiting or throttling - limits the number of request a client can send in a specified amount of time.
Throttled requests can be rejected immediately or their execution can be delayed.

- Q: Why not auto-scaling?

- A: Auto-scaling - up or out - does not happen immediately and by the time the process is complete, our service might already be dead.
Why not using Load

- Q: Why not use a load balancer to limit the number of max. connections or the max. threads count on the service endpoint?
- A: Load balancers can indeed limit the load that reaches a service instance, but they are **indiscriminate**.
The service can expose multiple operations - some fast, some slow - but the load balancer does not know anything about the service implementation.

- Q: But is this a system design problem?
- A: In a ideal world, it should not be.
But in the real world, load balancers don't distribute traffic equally.
When the number of requests is sensible - the service is paid or limited based on account tier, our servers need to exchange data about how many client requests each one has handled.


### Requirements

Functional:
- Implement allowRequest(request)

Non-Functional (NFR):
- Low latency - decision is made asap
- Accurate - as accurate as we can get
- Scalable - scales out with the number of hosts we add
- Ease of integration - accessible to teams as seamlessly as possible

### Design

#### Single Server
![Single Server Rate Limiter Design](https://drive.google.com/uc?id=1GspHTuTicqFkai5I9j0pRxDnWEhzqz6y)

#### Components
* **Rules service**
    - a rule represents the **nr. of req. per time interval** that a client can send
    - allows admins to manage rate limiting rules
    - rules are stored in the **Rules DB**

* **Rules retriever** 
    - Background process that polls **Rules service** periodically to check if there are any new or changed rules
    - Retrieved rules are stored in the **Rules Cache**
      
* **Client ID Builder**
    - identifies clients by assigning them an ID or key
    - the ID can be a token, remote address or combination of attr. that uniquely identifies the client
    - passes the key to the **Rate Limiter**
    - can be merged in the **Rate Limiter**
    
* **Rate Limiter**
    - makes the limit decision
    - checks key against rules
    - if the threshold is not exceeded the request is processed
    - otherwise, the request can be **dropped**, **queued** or **rejected**
    
#### Algorithm
There are multiple algorithms that can be applied for solving this problem. See Google Guava's library.
Also the fixed and sliding window paradigm can be a good place to start.

##### The Token Bucket algorithm
A bucket can hold multiple tokens and every time a request comes, it takes a token from the bucket.
The bucket is also refilled at a fixed rate.
Operations that are more **expensive** can have a cost **bigger** than one token. 

Each bucket has 3 characteristics: 
- maximum bucket capacity
- current available tokens
- refill rate - the rate at which the tokens are added to the bucket

### Implementation

In the **rate-limiter-service** folder, we have both the framework interfaces and an example implementation.

#### Framework
* **IClientId** - identifies a client. Uniqueness is achieved via implementation.
* **IClientIdBuilder** - generates ID's for clients.
* **IRule** - in this design, a rule is constructed based on a client's type, the max. requests per time frame and the length of the time frame
* **IRulesCache** - maintains a store of cached rules. Should provide sync mechanics and querying by ID
* **IRetrieverJobScheduler** - manages rule retrieving/sync tasks
* **IRuleRetrieverTask** - implements rule sync logic
* **IRateLimiter** - verifies client request based on ID

#### Example implementation
* **ClientId < IClientId** - holds client identity

* **ClientIdBuilder < IClientIdBuilder** - build client ID's using client tokens, client type and remote addresses

* **Rule < IRule** - simple implementation of IRule

* **RulesCache < IRulesCache** - uses a ConcurrentHashMap for caching. Keys are client types. In this example implementation, there is one rule per client.
E.g. desktop clients could have more requests allowed than mobile clients 

* **RetrieverJobScheduler < IRetrieverJobScheduler** - used a ScheduledExecutorService to run a IRuleRetrieverTask after each 10 seconds

* **RuleRetrieverTask < IRuleRetrieverTask** - has access to both a rule cache and a rule service so it can update the cache whenever the task is run by the executor.

* **RateLimiterManager** - Bean that starts the rule retriever scheduler.

* **RuleService** - is just a dummy service that gets rules from a mock provider that returns a list of rules. Each rule is a list: client_type, timeframe, number of requests.
For simplicity, the timeframe is always 1s.

* **TokenBucketRateLimiter < IRateLimiter** - implements the rate limiting policy using TokenBuckets.
Whenever a new client reaches the service, a TokenBucket is associated with it's ID via a call to **trackClient**.
The bucket is configured according to the rules for that specific client.
Buckets are stored in a ConcurrentHashMap with ID's as keys.
The actual accept/decline decision logic is inside the TokenBucket.

* **TokenBucket** - contains the rate limiting policy details for a specific client including:
    * maxBucketSize - the maximum number of tokens it hold at one point in time
    * refillRatePerSecond - the amount of tokens that become available with each second. In our simple scenario, the bucket is fully refilled after each second.
    * currentBucketSize - tokens currently available
    * lastRefillTimestamp - timestamp of the last refil operation.
    * **allowRequest** is called for each request - by default each request costs 1 token.
    We first refill the bucket and set the current available capacity.
    If tokens are available, we allow the request, otherwise we deny it.
    
* **ServiceableEndpoint** - the entry point to our application. Holds the rate limited resource endpoint.
    


