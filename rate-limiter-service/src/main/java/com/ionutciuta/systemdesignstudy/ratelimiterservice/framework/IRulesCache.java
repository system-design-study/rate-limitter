package com.ionutciuta.systemdesignstudy.ratelimiterservice.framework;

import java.util.List;

public interface IRulesCache<R extends IRule, ID extends IClientId> {
    void syncRules(List<R> rules);
    IRule getApplicableRule(ID id);
}
