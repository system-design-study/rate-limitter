package com.ionutciuta.systemdesignstudy.ratelimiterservice.framework;

public interface IRetrieverJobScheduler {
    void start(IRuleRetrieverTask task);
    void stop();
}
