package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRetrieverJobScheduler;
import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRuleRetrieverTask;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RetrieverJobScheduler implements IRetrieverJobScheduler {
    private static final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

    @Override
    public void start(IRuleRetrieverTask task) {
        executor.scheduleAtFixedRate(task, 0, 10, TimeUnit.SECONDS);
    }

    @Override
    public void stop() {
        executor.shutdown();
    }
}
