package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

public class TokenBucket {
    private final long maxBucketSize;
    private final double refillRatePerSecond;

    private double currentBucketSize;
    private long lastRefillTimestamp;

    public TokenBucket(long maxBucketSize, double refillRatePerSecond) {
        this.maxBucketSize = maxBucketSize;
        this.refillRatePerSecond = refillRatePerSecond;
        this.currentBucketSize = maxBucketSize;
        this.lastRefillTimestamp = System.currentTimeMillis();
    }

    private synchronized boolean allowRequest(int tokens) {
        boolean allowed = false;

        refill();

        if(currentBucketSize - tokens >= 0) {
            currentBucketSize -= tokens;
            allowed = true;
        }

        return allowed;
    }

    public synchronized boolean allowRequest() {
        return allowRequest(1);
    }

    private void refill() {
        long now = System.currentTimeMillis();
        double accumulatedTokens = (now - lastRefillTimestamp) / 1000d * refillRatePerSecond;
        currentBucketSize = Math.min(maxBucketSize, currentBucketSize + accumulatedTokens);
        lastRefillTimestamp = now;
    }

    @Override
    public String toString() {
        return String.format(
                "TokenBucket - maxRequests: %s, refillRatePerSecond: %s, currentBucketSize: %s, lastRefillTimestamp: %s",
                maxBucketSize, refillRatePerSecond, currentBucketSize, lastRefillTimestamp
        );
    }
}
