package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRule;

public class Rule implements IRule {
    private String clientType;
    private int maxRequestsPerTimeWindow;
    private int timeWindowInSeconds;

    public Rule(String clientType, int maxRequestsPerTimeWindow, int timeWindowInSeconds) {
        this.clientType = clientType;
        this.maxRequestsPerTimeWindow = maxRequestsPerTimeWindow;
        this.timeWindowInSeconds = timeWindowInSeconds;
    }

    @Override
    public String getClientType() {
        return this.clientType;
    }

    @Override
    public int getMaxRequestsPerTimeWindow() {
        return this.maxRequestsPerTimeWindow;
    }

    @Override
    public int getTimeWindow() {
        return this.timeWindowInSeconds;
    }

    @Override
    public String toString() {
        return String.format(
                "Rule - clientType: %s, maxRequest: %s, timeWindowInSeconds: %s",
                clientType, maxRequestsPerTimeWindow, timeWindowInSeconds
        );
    }
}
