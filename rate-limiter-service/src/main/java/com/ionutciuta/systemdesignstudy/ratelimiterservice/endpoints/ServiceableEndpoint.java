package com.ionutciuta.systemdesignstudy.ratelimiterservice.endpoints;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IClientId;
import com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation.ClientIdBuilder;
import com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation.TokenBucketRateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/limited")
public class ServiceableEndpoint {
    private static final Logger log = LoggerFactory.getLogger(ServiceableEndpoint.class);

    @Autowired
    private ClientIdBuilder clientIdBuilder;

    @Autowired
    private TokenBucketRateLimiter tokenBucketRateLimiter;

    @GetMapping
    public ResponseEntity<String> get(@RequestHeader(name = "X-Client-Type") String type,
                                      @RequestHeader(name = "X-Client-Token") String token,
                                      HttpServletRequest request) {
        IClientId clientId = clientIdBuilder.build(type, token, request.getRemoteAddr());
        log.info("Servicing request for client {}", clientId.getId());

        if (!tokenBucketRateLimiter.allowRequest(clientId)) {
            return new ResponseEntity<>("Too many requests!", HttpStatus.TOO_MANY_REQUESTS);
        }

        return ResponseEntity.ok("Allowed!");
    }
}
