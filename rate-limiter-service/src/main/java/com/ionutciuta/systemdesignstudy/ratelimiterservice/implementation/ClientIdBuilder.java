package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IClientId;
import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IClientIdBuilder;
import org.springframework.stereotype.Component;

@Component
public class ClientIdBuilder implements IClientIdBuilder {

    @Override
    public IClientId build(Object... input) {
        String type = (String) input[0];
        String token = (String) input[1];
        String remoteAddr = (String) input[2];
        String id = String.format("%s-%s-%s", type, token, remoteAddr);
        return new ClientId(type, token, remoteAddr, id);
    }
}
