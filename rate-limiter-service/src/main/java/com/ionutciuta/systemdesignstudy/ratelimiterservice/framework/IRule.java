package com.ionutciuta.systemdesignstudy.ratelimiterservice.framework;

public interface IRule {
    String getClientType();
    int getMaxRequestsPerTimeWindow();
    int getTimeWindow();
}
