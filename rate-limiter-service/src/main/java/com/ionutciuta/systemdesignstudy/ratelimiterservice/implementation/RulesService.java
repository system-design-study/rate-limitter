package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.mocks.MockRulesProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RulesService {
    private static final Logger log = LoggerFactory.getLogger(RulesService.class);

    public List<Rule> getRules() {
        List<Object> rulesObj = MockRulesProvider.getInstance().provideRules();
        log.info("Retrieved rules: {}", rulesObj);
        return rulesObj
                .stream()
                .map(ruleObjDef -> convertRuleDefToRule((List)ruleObjDef))
                .collect(Collectors.toList());

    }

    private Rule convertRuleDefToRule(List ruleDef) {
        String clientType = (String) ruleDef.get(0);
        Integer maxRequests = (Integer) ruleDef.get(1);
        Integer time = (Integer) ruleDef.get(2);
        return new Rule(clientType, maxRequests, time);
    }
}
