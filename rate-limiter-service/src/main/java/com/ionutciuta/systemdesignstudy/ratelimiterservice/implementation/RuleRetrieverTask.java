package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRuleRetrieverTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class RuleRetrieverTask implements IRuleRetrieverTask<Rule> {
    private static final Logger log = LoggerFactory.getLogger(RulesCache.class);

    private RulesService rulesService;
    private RulesCache rulesCache;

    public RuleRetrieverTask(RulesService rulesService, RulesCache rulesCache) {
        this.rulesService = rulesService;
        this.rulesCache = rulesCache;
    }

    @Override
    public List<Rule> fetch() {
        List<Rule> rules = rulesService.getRules();
        log.info("Fetched new rules: {}", rules);
        return rules;
    }

    @Override
    public void run() {
        rulesCache.syncRules(fetch());
    }
}
