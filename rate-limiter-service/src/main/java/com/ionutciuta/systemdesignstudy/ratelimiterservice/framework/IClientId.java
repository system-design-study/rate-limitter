package com.ionutciuta.systemdesignstudy.ratelimiterservice.framework;

public interface IClientId {

    public String getId();
}
