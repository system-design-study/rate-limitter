package com.ionutciuta.systemdesignstudy.ratelimiterservice.framework;

public interface IClientIdBuilder {
    IClientId build(Object... input);
}
