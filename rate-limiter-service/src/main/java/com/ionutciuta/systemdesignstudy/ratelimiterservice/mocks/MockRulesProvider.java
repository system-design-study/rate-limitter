package com.ionutciuta.systemdesignstudy.ratelimiterservice.mocks;

import java.util.List;

public class MockRulesProvider {

    private MockRulesProvider() {}

    private static MockRulesProvider instance;

    public static synchronized MockRulesProvider getInstance() {
        if(instance == null) {
            instance = new MockRulesProvider();
        }

        return instance;
    }

    public List provideRules() {
        return List.of(
                List.of("mobile", 1, 10),
                List.of("web", 1, 5)
        );
    }
}
