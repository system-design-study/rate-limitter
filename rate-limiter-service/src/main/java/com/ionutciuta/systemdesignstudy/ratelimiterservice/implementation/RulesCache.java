package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRule;
import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRulesCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class RulesCache implements IRulesCache<Rule, ClientId> {
    private static final Logger log = LoggerFactory.getLogger(RulesCache.class);

    private Map<String, Rule> rulesCache = new ConcurrentHashMap<>();

    @Override
    public void syncRules(List<Rule> rules) {
        log.info("Syncing rules. Got {}", rules);
        rules.forEach(rule -> rulesCache.put(rule.getClientType(), rule));
    }

    @Override
    public IRule getApplicableRule(ClientId id) {
        return rulesCache.get(id.getType());
    }

    public Set<Rule> getAllRules() {
        return Set.copyOf(rulesCache.values());
    }
}
