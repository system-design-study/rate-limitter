package com.ionutciuta.systemdesignstudy.ratelimiterservice.framework;

import java.util.List;

public interface IRuleRetrieverTask<R extends IRule> extends Runnable {
    List<R> fetch();
}
