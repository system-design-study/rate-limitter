package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.RateLimiterServiceApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class RateLimiterManager {
    private static final Logger log = LoggerFactory.getLogger(RateLimiterServiceApplication.class);

    @Autowired
    private RulesService rulesService;

    @Autowired
    private RulesCache rulesCache;

    private final RetrieverJobScheduler jobScheduler = new RetrieverJobScheduler();

    @EventListener
    public void onStart(ContextRefreshedEvent startCtxEvent) {
        log.info("Context Start Event received. Starting RetrieverJobScheduler.");
        jobScheduler.start(new RuleRetrieverTask(rulesService, rulesCache));
    }

    @EventListener
    public void onStop(ContextStoppedEvent stopCtxEvent) {
        log.info("Context Start Event received. Stopping RetrieverJobScheduler");
        jobScheduler.stop();
    }
}
