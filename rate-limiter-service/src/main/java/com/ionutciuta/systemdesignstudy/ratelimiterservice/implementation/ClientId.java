package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IClientId;

public class ClientId implements IClientId {

    private String type;
    private String token;
    private String remoteAddress;
    private String id;

    public ClientId(String type, String token, String remoteAddress, String id) {
        this.type = type;
        this.token = token;
        this.remoteAddress = remoteAddress;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public String getRemoteAddress() {
        return remoteAddress;
    }

    @Override
    public String getId() {
        return id;
    }
}
