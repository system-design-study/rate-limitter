package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IClientId;
import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRateLimiter;
import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRule;
import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRulesCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class TokenBucketRateLimiter implements IRateLimiter {
    private static final Logger log = LoggerFactory.getLogger(TokenBucketRateLimiter.class);
    private static final Map<String, TokenBucket> buckets = new ConcurrentHashMap<>();

    @Autowired
    private IRulesCache rulesCache;

    @Override
    public boolean allowRequest(IClientId clientId) {
        ClientId id = (ClientId) clientId;

        log.info("Validating request for client {}", id.getId());

        if(!buckets.containsKey(id.getId())) {
            trackClient(id);
        }

        return checkClientRequest(id);
    }

    public void trackClient(ClientId clientId) {
        IRule applicableRule = rulesCache.getApplicableRule(clientId);
        log.info("Client {} not tracked. Tracking by rule {}.", clientId.getId(), applicableRule);
        double defaultRefillRate = applicableRule.getMaxRequestsPerTimeWindow() * 1d / applicableRule.getTimeWindow();
        buckets.put(clientId.getId(), new TokenBucket(applicableRule.getMaxRequestsPerTimeWindow(), defaultRefillRate));
    }

    public boolean checkClientRequest(ClientId clientId) {
        TokenBucket tokenBucket = buckets.get(clientId.getId());
        log.info("Client {} has bucket: {}", clientId.getId(), tokenBucket);
        return tokenBucket.allowRequest();
    }

}
