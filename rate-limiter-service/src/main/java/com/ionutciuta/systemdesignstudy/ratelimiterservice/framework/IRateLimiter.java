package com.ionutciuta.systemdesignstudy.ratelimiterservice.framework;

public interface IRateLimiter {
    boolean allowRequest(IClientId id);
}
