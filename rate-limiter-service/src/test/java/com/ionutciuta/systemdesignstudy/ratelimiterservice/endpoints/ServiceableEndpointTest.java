package com.ionutciuta.systemdesignstudy.ratelimiterservice.endpoints;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ServiceableEndpointTest {
    private static final String MOBILE_CLIENT = "mobile";
    private static final String WEB_CLIENT = "web";
    private static final String DUMMY_TOKEN = "abc123";
    private static final URI ENDPOINT_URI = URI.create("/limited");

    @Autowired
    private TestRestTemplate testClient;

    @Test
    void rateLimiterShouldAllowCorrectlySpacedRequestForMobileClient() throws Exception {
        RequestEntity entity = new RequestEntity<>(
                getHeaders(MOBILE_CLIENT, DUMMY_TOKEN),
                HttpMethod.GET,
                ENDPOINT_URI
        );

        String response1 = testClient.exchange(entity, String.class).getBody();

        Thread.sleep(10 * 1000);

        String response2 = testClient.exchange(entity, String.class).getBody();

        Thread.sleep(5 * 1000);

        HttpStatus status = testClient.exchange(entity, String.class).getStatusCode();

        assertEquals("Allowed!", response1);
        assertEquals("Allowed!", response2);
        assertEquals(HttpStatus.TOO_MANY_REQUESTS, status);
    }

    @Test
    void rateLimiterShouldAllowCorrectlySpacedRequestForWebClient() throws Exception {
        RequestEntity entity = new RequestEntity<>(
                getHeaders(WEB_CLIENT, DUMMY_TOKEN),
                HttpMethod.GET,
                ENDPOINT_URI
        );

        String response1 = testClient.exchange(entity, String.class).getBody();

        Thread.sleep(5 * 1000);

        String response2 = testClient.exchange(entity, String.class).getBody();

        Thread.sleep(2 * 1000);

        HttpStatus status = testClient.exchange(entity, String.class).getStatusCode();

        assertEquals("Allowed!", response1);
        assertEquals("Allowed!", response2);
        assertEquals(HttpStatus.TOO_MANY_REQUESTS, status);
    }

    private HttpHeaders getHeaders(String clientType, String clientToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Client-Type", clientType);
        headers.add("X-Client-Token", clientToken);
        return headers;
    }
}