package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RuleRetrieverTaskTest {

    @Test
    public void fetchShouldPopulateCache() throws InterruptedException {
        RulesCache cache = new RulesCache();
        RulesService service = new RulesService();
        RuleRetrieverTask task = new RuleRetrieverTask(service, cache);

        Thread thread = new Thread(task);
        thread.start();

        System.out.println("Fetching...");
        while (thread.isAlive()) {
            // just wait
        }
        System.out.println("Fetch complete");
        thread.join();

        assertEquals(2, cache.getAllRules().size());
    }

}