package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RulesServiceTest {

    @Test
    void getRules() {
        RulesService rulesService = new RulesService();
        List<Rule> rules = rulesService.getRules();

        assertEquals(2, rules.size());
        assertEquals("mobile", rules.get(0).getClientType());
        assertEquals("web", rules.get(1).getClientType());
    }
}