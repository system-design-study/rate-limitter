package com.ionutciuta.systemdesignstudy.ratelimiterservice.implementation;

import com.ionutciuta.systemdesignstudy.ratelimiterservice.framework.IRuleRetrieverTask;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

class RetrieverJobSchedulerTest {

    @Test
    public void testScheduler() {
        RetrieverJobScheduler scheduler = new RetrieverJobScheduler();
        final AtomicInteger counter = new AtomicInteger(0);

        scheduler.start(new IRuleRetrieverTask() {
            @Override
            public List fetch() {
                System.out.println("Fetching...");
                counter.getAndIncrement();
                return List.of();
            }

            @Override
            public void run() {
                fetch();
            }
        });

        while (true) {
            if (counter.get() == 2)
                break;
        }

        scheduler.stop();
        assertEquals(2, counter.get());
    }

}